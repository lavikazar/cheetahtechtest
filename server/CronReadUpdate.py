import os
import schedule
import logging
from DataEngine import MsSqlServer


class CronJobCatalog:

    logger = logging.getLogger('piLogger' + "." + __name__)

    def __init__(self, base_path, cron_job_time, server_name, database, username, password, csv_file):
        CronJobCatalog.logger.info('Started the scheduler cron job of reading csv catalog and updating DB')
        schedule.every().day.at(cron_job_time).do(self.cron_job)
        self.servername = server_name
        self.database = database
        self.username = username
        self.password = password
        self.dw = MsSqlServer(self.servername, self.database, self.username, self.password, csv_path=os.path.join(base_path, csv_file))

    def cron_job(self):
        CronJobCatalog.logger.info('cron job fired')

        # Update the DB with the new data
        con = self.dw.connect_db()
        if con:
            self.dw.update_db(con)
