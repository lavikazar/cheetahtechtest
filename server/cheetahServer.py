import os
import sys
import logging.config
from PaginationMech import Pagination
from ConfigurationMgr import Config
from CronReadUpdate import CronJobCatalog
from flask import Flask, render_template
from DataEngine import MsSqlServer

global logger
global sql

PER_PAGE = 100

app = Flask(__name__)
app.config['TRAP_HTTP_EXCEPTIONS'] = True
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 10


@app.route("/Producers/<producer>/Products/", defaults=dict(page=1))
@app.route("/Producers/<producer>/Products/page/<int:page>")
def get_products_by_producer(producer, page):
    logger.info("Start getting products by producer")
    con = sql.connect_db()
    products = sql.get_products_by_user(con, producer, page, PER_PAGE)
    if not products and page != 1:
        return 404
    count = sql.get_products_by_user_count(con, producer)
    pagination = Pagination(page, PER_PAGE, count)
    logger.info("Successfully finish getting products by producer for page: {}".format(page))

    # This is the page which we direct our products and pagination arguments
    # Then in the front end when the user request the second page we get to the
    # get_products_by_producer function and return the next PER_PAGE products
    return render_template('products.html',
                           pagination=pagination,
                           products=products
                           )


if __name__ == "__main__":
    try:
        base_path = os.path.dirname(os.path.abspath(__file__))

        # Reading configuration settings
        configCron = Config(base_path)

        # logs the system behavior
        log_file = configCron.get('CronJob Configuration', 'log_file')
        logging.config.fileConfig(os.path.join(base_path, log_file))
        logger = logging.getLogger('root.' + __name__)

        # Initializing the cron job
        cron_job_time = configCron.get('CronJob Configuration', 'cron_job_time')
        csv_file = configCron.get('CronJob Configuration', 'csv_file')
        server_name = configCron.get('Env Configuration', 'server_name')
        database = configCron.get('Env Configuration', 'database')
        username = configCron.get('Env Configuration', 'username')
        password = configCron.get('Env Configuration', 'password')
        CronJobCatalog(base_path, cron_job_time, server_name, database, username, password, csv_file)

        sql = MsSqlServer(server_name, database, username, password)

        logger.info('Start running server on port:8110, host: localhost')
        sys.exit(app.run(port=8110, threaded=True, host='0.0.0.0'))
    except:
        logger = logging.getLogger('Logger.{}'.format(__name__))
        logger.exception('!!!Server stops unexpectedly!!!')
