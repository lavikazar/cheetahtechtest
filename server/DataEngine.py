import pandas as pd
import logging
import pyodbc


class MsSqlServer:

    logger = logging.getLogger('piLogger' + "." + __name__)

    def __init__(self, server_config, database, username, password, csv_path = ''):
        self.server_name = server_config
        self.database = database
        self.username = username
        self.password = password
        self.csv_path = csv_path

    def connect_db(self):
        try:
            MsSqlServer.logger.info('Connecting to Sql Server DB: servername:{}, database={}'.format(self.server_name, self.database))
            if self.username != 'None' and self.password != 'None':
                con = pyodbc.connect('DRIVER={{SQL Server}};SERVER={};DATABASE={};UID={};PWD={}'.format(self.server_name, self.database, self.username, self.password))
            else:
                con = pyodbc.connect('DRIVER={{SQL Server}};SERVER={};DATABASE={}'.format('0.0.0.0', self.database))
            cursor = con.cursor()
            MsSqlServer.logger.info('Successfully connected to DB')
            return cursor
        except:
            MsSqlServer.logger.info('Failed connecting to DB')
            return None

    def update_db(self, con):
        try:
            con.execute("exec SP_ReadUpdate_Catalog {}".format(self.csv_path))
            con.commit()
        except:
            MsSqlServer.logger.info('Failed updating the cheetah catalog')

    def get_products_by_user(self, con, producer, page, per_page):
        try:
            con.execute("exec SP_get_products_by_producer {}, {}, {}".format(producer, (page-1)*per_page, per_page))
            result = con.fetchall()
            return result
        except:
            MsSqlServer.logger.info('Failed updating the cheetah catalog')
            return None

    def get_products_by_user_count(self, con, producer):
        try:
            con.execute("exec SP_get_products_by_procuder_count {}".format(producer))
            result = con.fetchall()
            return result
        except:
            MsSqlServer.logger.info('Failed reading number of products per producer')
            return None
