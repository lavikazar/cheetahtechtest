import os
import ConfigParser


class Config:

    def __init__(self, base_path):
        self.configCron = ConfigParser.SafeConfigParser()
        self.configCron.read(os.path.join(base_path, 'Configurations\\settings.ini'))

    def get(self, section, item):
        return self.configCron.get(section, item)
