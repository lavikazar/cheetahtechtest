-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SP_get_products_by_producer 
	-- Add the parameters for the stored procedure here
	@producer varchar(100),
	@Skip int,
	@Take int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select unique_id, product_name, barcode, photo_url, price_cents
	from Catalog_Cheetah
	where producer = @producer
	order by unique_id
	OFFSET (@Skip) ROWS FETCH NEXT (@Take) ROWS ONLY
END
GO
