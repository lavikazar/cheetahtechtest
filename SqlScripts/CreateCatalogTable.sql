use master

IF OBJECT_ID('Catalog_Cheetah') is not null drop table Catalog_Cheetah

CREATE TABLE Catalog_Cheetah (
    unique_id nvarchar(50),
    product_name nvarchar(255),
    barcode nvarchar(50),
    photo_url nvarchar(200),
	price_cents nvarchar(50),
	producer nvarchar(100)
);