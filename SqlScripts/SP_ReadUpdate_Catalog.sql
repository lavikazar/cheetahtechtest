-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SP_ReadUpdate_Catalog
	-- Add the parameters for the stored procedure here
	@csv_path nvarchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--Creating view products
	print 'Creating view products...'
	DROP TABLE IF EXISTS #VW_products
	CREATE TABLE #VW_products (
		product_name nvarchar(255),
		photo_url nvarchar(200),
		barcode nvarchar(50),
		unique_id nvarchar(50),
		price_cents nvarchar(50),
		producer nvarchar(100)
	);
	
	--Reading csv catalog file
	print 'Reading the csv into the view products...'
	declare @sqlcommand varchar(1000) set @sqlcommand = 
	'bulk insert #VW_products
	from '''+ @csv_path +'''
	WITH (FORMAT=''CSV'');'
	print 'Executing: ' + @sqlcommand
	exec(@sqlCommand)

	print 'Truncating data from catalog_cheetah'
	truncate table Catalog_Cheetah

	print 'Insert updated data into Catalog Cheetah'
	insert into Catalog_Cheetah
	select unique_id, product_name, barcode, photo_url, price_cents, producer from #VW_products
	print 'Successfuly updated data into Catalog Cheetah'
END
GO
